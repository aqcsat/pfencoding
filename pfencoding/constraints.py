"""Classes representing constraints over penalty functions"""
from itertools import product, combinations
from typing import Iterable, Optional, Tuple, Callable
from pfencoding.penaltyfunction import PenaltyFunction
from abc import abstractmethod
from smtutils.formula import ValidNode, Symbol


def isingexpansion(n):
    # type: (int) -> Iterable
    "Return all possible states of a n-var Ising model"
    return product(*([(-1, 1)] * n))


def isingtobool(x):
    # type: (int) -> bool
    "Convert from (-1, 1) to (False, True)"
    return x == 1


class PfConstraint(object):
    "Abstract class representing a constraint over a pf"

    @abstractmethod
    def constraints(self, pf):
        # type: (PenaltyFunction) -> Iterable[ValidNode]
        "Generate the constraint for a specific pf"
        raise NotImplementedError


class RangeConstraint(PfConstraint):
    "Class representing range constraints on parameters"

    def __init__(self, biasrange=(-2, 2), couplrange=(-1, 1)):
        # type: (Optional[Tuple[int,int]], Optional[Tuple[int,int]]) -> None
        self.biarange = biasrange
        self.couplrange = couplrange

    def constraints(self, pf):
        biasmin, biasmax = self.biarange
        cmin, cmax = self.couplrange
        for bias in pf.biases:
            yield ((bias >= biasmin) & (bias <= biasmax))
        for coupl in pf.couplings:
            yield ((coupl >= cmin) & (coupl <= cmax))


class ArchitectureConstraint(PfConstraint):
    "Class representing constraints on the induced graph"

    def __init__(self, edges):
        # type: (Iterable[Tuple[ValidNode, ValidNode]]) -> None
        self.edges = frozenset(edges)

    def constraints(self, pf):
        edges = (self.edges)
        for i, coup in enumerate(combinations(pf.zs, 2)):
            if coup not in edges:
                yield (pf.couplings[i] == 0)


class ExpansionGapConstraint(PfConstraint):
    """Generate gap constraints by expanding variables"""

    def __init__(self, boolfunc, gmin):
        # type: (Callable[Iterable[bool], bool], ValidNode) -> None
        self.boolfunc = boolfunc
        self.gmin = gmin

    def constraints(self, pf):
        """As an iterator for speed, yields constraints for every possible assignments on xs an as"""
        boolfunc = self.boolfunc
        gmin = self.gmin
        for xvals in isingexpansion(pf.nx):
            if boolfunc(map(isingtobool, xvals)):
                allpfs = []
                for avals in isingexpansion(pf.na):
                    asfull = pf.value(xvals + avals)
                    yield (asfull >= 0)
                    allpfs.append(asfull == 0)
                if pf.na == 0:
                    yield (pf.value(xvals) == 0)
                else:
                    yield reduce(lambda a, b: a | b, allpfs)
            else:
                for avals in isingexpansion(pf.na):
                    yield (pf.value(xvals + avals) >= gmin)
                if pf.na == 0:
                    yield (pf.value(xvals) >= gmin)


class MinimizeAncillas(PfConstraint):
    """Add a symbol counting the number of ancilla used"""

    def __init__(self):
        self.au = Symbol("Int", "ancilla_used")

    def constraints(self, pf):
        """Constraints that define the number of ancilla used"""
        ancillaused = self.au
        yield ancillaused >= 0
        nx = pf.nx
        ncoupl = list(combinations(range(len(pf.zs)), 2))
        for i in range(pf.na):
            zi = nx + i
            yield ~(ancillaused <= i) | ((pf.biases[zi] == 0) &
                                         reduce(lambda a, b: a & b,
                                                [(coupl == 0) for coupl, (i, j) in zip(pf.couplings, ncoupl)
                                                 if i == zi or j == zi]))

    def minimize(self):
        """Variables to minimize"""
        return [self.au]
