"""Utility functions to find penalty functions"""
from smtutils.formula import SmtFormula
from smtutils.process import Solver, get_msat_path
from smtutils.parsing import SmtResponseParser
from pfencoding.constraints import RangeConstraint, ExpansionGapConstraint, ArchitectureConstraint, MinimizeAncillas, \
    PfConstraint
from pfencoding.penaltyfunction import PenaltyFunction
from pfencoding.movablepf import MovableArchConstraints, MovablePenaltyFunction
from typing import Callable, Optional, List
import networkx


def search_pf(nx, na, graph, func):
    # type: (int, int, Optional[networkx.Graph], Callable) -> (dict, PenaltyFunction)
    """Search for a fixed-graph penalty function"""
    f = SmtFormula()
    pf = PenaltyFunction(nx, na)
    constr_types = [RangeConstraint(), ExpansionGapConstraint(func, 2)]
    if graph is not None:
        archcon = ArchitectureConstraint(graph.edges_iter())
        constr_types.append(archcon)
    for ctype in constr_types:
        for constr in ctype.constraints(pf):
            f.assert_(constr)

    slv = Solver(get_msat_path("optimathsat"))
    f.check_sat()
    f.get_values(*pf.thetas)
    res = slv.run_formula(str(f))
    resp = SmtResponseParser(res)
    model = resp.model

    return model, pf


def search_pf_movable(nx, na, graph, func):
    # type: (int, int, networkx.Graph, Callable) -> (dict, PenaltyFunction)
    """Search for a penalty function with variable placement"""
    pf = MovablePenaltyFunction(nx, na)
    nodes = [i + 1 for i in graph.nodes_iter()]
    edges = [(i + 1, j + 1) for i, j in graph.edges_iter()]
    constraints = [
        RangeConstraint(),
        MovableArchConstraints(nodes, edges),
        ExpansionGapConstraint(func, 2)
    ]

    formula = SmtFormula()

    for cfact in constraints:
        for costraint in cfact.constraints(pf):
            formula.assert_(costraint)

    formula.check_sat()
    formula.get_values(*pf.thetas)
    formula.get_values(*pf.pos)

    solver = Solver(get_msat_path("optimathsat"))
    res = solver.run_formula(str(formula))

    pres = SmtResponseParser(res)
    model = pres.model
    return model, pf


def search_pf_smallest(nx, na, graph, func, extraconstr=tuple()):
    # type: (int, int, networkx.Graph, Callable, Optional[List[PfConstraint]]) -> (dict, PenaltyFunction)
    """Search for the smallest2 penalty function with variable placement"""
    pf = MovablePenaltyFunction(nx, na)
    nodes = [i + 1 for i in graph.nodes_iter()]
    edges = [(i + 1, j + 1) for i, j in graph.edges_iter()]
    ma = MinimizeAncillas()
    constraints = [
        RangeConstraint(),
        MovableArchConstraints(nodes, edges),
        ExpansionGapConstraint(func, 2),
        ma
    ]
    constraints.extend(extraconstr)

    formula = SmtFormula()

    for cfact in constraints:
        for costraint in cfact.constraints(pf):
            formula.assert_(costraint)

    formula.minimize(ma.au)

    formula.check_sat()
    formula.directives.append("(set-model -1)")
    formula.get_values(*pf.thetas)
    formula.get_values(*pf.pos)
    formula.get_values(ma.au)

    solver = Solver(get_msat_path("optimathsat"))
    res = solver.run_formula(str(formula))

    # nothing to parse omt comments yet
    res = "\n".join(x for x in res.splitlines() if not x.startswith("#"))
    pres = SmtResponseParser(res)
    model = pres.model
    return model, pf
