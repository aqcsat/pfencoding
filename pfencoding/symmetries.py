from .constraints import PfConstraint
from smtutils.formula import Op, Symbol
from itertools import  combinations


class SingleTileSymBreak(PfConstraint):
    """Quick&dirty symmetry breaking in a single tile"""
    def constraints_old(self, pf):
        yield  pf.pos[0] == 1
        yield  (pf.pos[1] == 2) | (pf.pos[1] == 5)
        yield (pf.pos[2] == 3) | (pf.pos[2] == 6)
        yield (pf.pos[3] == 4) | (pf.pos[3] == 7)

    def constraints(self, pf):
        yield  pf.pos[0] == 1
        possible_placements = []
        for t in combinations(range(1,8), 3):
            top = [pf.pos[x] for x in t]
            plac = (top[0] == 2) & (top[1] == 3) & (top[2] == 4)
            bot = [pf.pos[x] for x in range(1,8) if x not in t]
            plac2 = (bot[0] == 5) & (bot[1] == 6) & (bot[2] == 7) & (bot[3] == 8)
            possible_placements.append(plac & plac2)
        yield Op("or", *possible_placements)



class ChimeraSymBreak(PfConstraint):
    def __init__(self, w, l):
        self.w = w
        self.l = l
        self.row = Symbol(("Int", "(Int)"), "row_id")
        self.is_row_occupied= Symbol(("Bool", "(Int Int)"), "is_row_occupied")
        self.canonicalpos = Symbol(("Int", "(Int Int)"),"canonical_pos")

    def index_to_row(self, x, y, i):
        inner_index = ((i-1) % 4) + 1
        if ((i-1) %8) < 4:
            return x*4  + inner_index
        else:
            return self.w*4 + y* 4+ inner_index

    def constraints(self, pf):
        #define row
        w, l = self.w, self.l
        for y in range(l):
            for x in range(w):
                first = 1 + x*8 + y*w*8
                for n in range(first, first+8):
                    yield self.row(n) == self.index_to_row(x,y, n)
        #define first pos
        if w == l: #distinguish square configurations from rectangular, todo: smth working for w,l > 2
            yield pf.pos[0] == 1
        else:
            yield (pf.pos[0] == 1) | (pf.pos[0] ==5)
        yield self.is_row_occupied(1, self.row(pf.pos[0]))

        for i, nextpos in enumerate(pf.pos[1:], start=2):
            #detect occupied rows
            for j in range(1, w*4+l*4+1):
                yield ~(self.is_row_occupied(i-1, j)) | self.is_row_occupied(i, j)
            yield self.is_row_occupied(i, self.row(nextpos))
            #define canonical
            for j in range(1,l*w*8+1):
                if j % 4 == 1:
                    yield self.canonicalpos(i,j) == j
                else:
                    yield self.canonicalpos(i, j) == Op("ite", self.is_row_occupied(i, self.row(j)),
                                                        j,
                                                        self.canonicalpos(i, j-1) )
            #assign nextpos to one of canonical
            possible_placements = [nextpos == self.canonicalpos(i,j) for j in range(1,l*w*8+1)]
            yield Op("or", *possible_placements)
            pass




class AncillaSymBreak(PfConstraint):
    """Sort ancillas by bias size to break order symmetry. This is valid even if some ancilla is not used
     (bias forced to 0), as ancilla are symmetric, all biases can be positive"""
    def constraints(self, pf):
        nx = pf.nx
        na = pf.na
        for i in xrange(nx, nx+na-1):
            yield pf.biases[i] >= pf.biases[i+1]
