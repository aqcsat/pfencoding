from itertools import combinations

from smtutils.formula import Symbol

from pfencoding.penaltyfunction import PenaltyFunction, vectproduct, vectcouplings
from pfencoding.constraints import PfConstraint


class MovablePenaltyFunction(PenaltyFunction):
    "Penalty functions with euf for positions."
    def define_spins(self, nx, na):
        # type: (int, int) -> None
        "Add pos to the variables definitions"
        super(MovablePenaltyFunction, self).define_spins(nx, na)
        self.pos = [Symbol("Int", "pos{}", i) for i in xrange(1, nx+na+1)]

    def define_theta(self, n):
        # type: (int) -> None
        """Define two functions for biases and couplings, and call them to recreate original theta variables
        \( \theta_{ij} \rightarrow \theta(i,j)\) """
        self.offset =  Symbol("Real", "off")
        self.bias_func = Symbol(("Real", "(Int)"), "bias")
        self.biases = [self.bias_func(posi) for posi in self.pos]
        self.coupl_func = Symbol(("Real", "(Int Int)"), "coupl")
        self.couplings = [self.coupl_func( i, j) for i,j in combinations(self.pos, 2)]




class MovableArchConstraints(PfConstraint):
    "Architecture constraints for euf penalty function"
    def __init__(self, nodes, edges):
        "Call with nodes and edges of arch graph"
        edges = frozenset(edges)
        assert set(xrange(1, 1+len(nodes))) == set(nodes), "Nodes must be consecutive ints 1..n"
        self.min = 1
        self.max = len(nodes)
        self.noedges = [e for e in combinations(nodes, 2) if e not in edges]

    def constraints(self, pf):
        "Assure that all variables are assigned to graph nodes and that all absent couplings are 0"
        for p1, p2 in combinations(pf.pos, 2):
            yield (p1 != p2)

        for i,j in combinations(range(1, self.max+1), 2):
            yield (pf.coupl_func(i,j) == pf.coupl_func(j,i))

        for p in pf.pos:
            yield (p >= self.min) & (p <= self.max)
        for i,j in self.noedges:
            yield (pf.coupl_func(i,j) == 0)
