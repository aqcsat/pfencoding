from unittest import TestCase
from pfencoding.searchpf import search_pf
from ..utils import print_pf


class TestExample2And2(TestCase):
    """Example encoding of and2 function"""

    def test_noancilla(self):
        func = (lambda (a, b, c): (c == (a and b)))

        model, pf = search_pf(3, 0, None, func)
        if model:
            print_pf(pf, model, func)

    def test_ancilla(self):
        func = (lambda (a, b, c): (c == (a and b)))

        model, pf = search_pf(3, 2, None, func)
        if model:
            print_pf(pf, model, func)
