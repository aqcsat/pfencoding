from unittest import TestCase
from pfencoding.searchpf import search_pf
from ..utils import print_pf
from chimerautils.chimera import create

class TestExample1in16(TestCase):
    """Example encoding of and2 function"""
    def test_ancilla(self):
        func = (lambda x: x.count(True) == 1)

        model, pf = search_pf(8, 8, create(1,2)[0], func)
        if model:
            print_pf(pf, model, func)
