from unittest import TestCase
from pfencoding.searchpf import search_pf_movable, search_pf_smallest
from chimerautils.chimera import create

from ..utils import print_pf


class TestExampleMaj3(TestCase):
    """Example encoding of majority function"""
    def test(self):
        g, _ = create(1,1)
        majfunc = lambda (a, b, c, d): (d == ((b or c) if a else (b and c)))

        model, pf = search_pf_movable(4,2, g, majfunc)

        print_pf(pf, model, majfunc)

    def test_smallest(self):
        g, _ = create(1,1)
        majfunc = lambda (a, b, c, d): (d == ((b or c) if a else (b and c)))

        model, pf = search_pf_smallest(4,2, g, majfunc)
        print model

        print_pf(pf, model, majfunc)








