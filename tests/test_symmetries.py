from unittest import TestCase
from pfencoding.movablepf import MovablePenaltyFunction
from pfencoding.symmetries import SingleTileSymBreak, ChimeraSymBreak
from chimerautils.chimera import create


class TestSymmetries(TestCase):
    def setUp(self):
        self.pf = MovablePenaltyFunction(5,3)

    def test1(self):
        pf = self.pf
        rc = SingleTileSymBreak()
        print map(str, rc.constraints(pf))


    def test2(self):
        pf = self.pf
        rc = ChimeraSymBreak(1,2)
        g, _ =create(1,2)
        print g.edges()
        for constr in map(str, rc.constraints(pf)):
            print constr