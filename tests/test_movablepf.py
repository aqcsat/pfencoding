from unittest import TestCase
from pfencoding.movablepf import MovableArchConstraints, MovablePenaltyFunction
from chimerautils.chimera import create

class MovableTest(TestCase):
    def test1(self):
        mpf = MovablePenaltyFunction(5, 3)
        g, _ = create(1,1)
        nodes = [i+1 for i in g.nodes_iter()]
        edges = [(i+1, j+1) for i,j in g.edges_iter()]
        ac = MovableArchConstraints(nodes, edges)
        print map(str, ac.constraints(mpf))
