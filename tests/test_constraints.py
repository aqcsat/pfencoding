from unittest import TestCase
from pfencoding.penaltyfunction import PenaltyFunction
from pfencoding.constraints import RangeConstraint, ExpansionGapConstraint, ArchitectureConstraint, MinimizeAncillas
from smtutils.formula import SmtFormula
from smtutils.process import Solver, get_msat_path
from smtutils.parsing import SmtResponseParser
from chimerautils.chimera import create


class TestConstraints(TestCase):
    def setUp(self):
        self.pf = PenaltyFunction(3,2)

    def test1(self):
        pf = self.pf
        rc = RangeConstraint()
        print map(str, rc.constraints(pf))

    def test2(self):
        pf = self.pf
        cs = ExpansionGapConstraint(lambda (a,b,c): c == (a and b), 2)
        print map(str, cs.constraints(pf))

    def test_arch(self):
        pf = PenaltyFunction(5,3)
        g, _ = create(1,1)
        edges = [ (pf.zs[i], pf.zs[j]) for i,j in g.edges_iter()]
        ac = ArchitectureConstraint(edges)
        print map(str, ac.constraints(pf))

    def test_minimize_ancillas(self):
        pf = PenaltyFunction(5, 5)
        mac = MinimizeAncillas()
        print map(str, mac.constraints(pf))


