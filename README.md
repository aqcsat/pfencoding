PfEncoding: penalty functions search framework
==============================================

Search a penalty function for a Boolean relation using SMT solvers.

example usage:

```python
from pfencoding import search_pf_smallest
import networx as nx

n_x = 5
n_a = 3

def relation(xs):
    return xs[0] == all(xs[1:])

graph = nx.complete_bipartite_graph(4, 4)

model, pf_object = search_pf_smallest(n_x, n_a, graph, relation)

print model


```