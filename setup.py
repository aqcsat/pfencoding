#!/usr/bin/env python2
from setuptools import find_packages, setup

setup(
    name='pfencoding',
    packages=find_packages(exclude=['tests']),
    install_requires=['smtutils', 'networkx', 'chimerautils'],
    test_suite='tests', )